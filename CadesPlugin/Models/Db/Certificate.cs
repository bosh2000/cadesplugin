﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CadesPlugin.Models.Db
{
    public class Certificate
    {
        public string Subject;
        public string Issuer;
        public string From;
    }
}
